module authmod

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/hashicorp/vault/api v1.0.4
	github.com/prometheus/client_golang v1.7.1
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.23.0

)
