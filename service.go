package main

import (
	pb "authmod/api"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"log"

	jwt "github.com/dgrijalva/jwt-go"
)

type AuthService struct {
	db map[string]*User
}

func NewAuthService() pb.AuthServiceServer {
	return &AuthService{
		db: map[string]*User{
			"veremchukvv@gmail.com": &User{
				ID:       1,
				Name:     "vladimir",
				Email:    "veremchukvv@gmail.com",
				Password: "4a83854cf6f0112b4295bddd535a9b3fbe54a3f90e853b59d42e4bed553c55a4",
			},
		},
	}
}

func (s *AuthService) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	mac := hmac.New(sha256.New, secret)
	mac.Write([]byte(req.Password))
	password := hex.EncodeToString(mac.Sum(nil))

	newUser := &User{
		Email:    req.Email,
		Name:     req.Name,
		Phone:    req.Phone,
		Age:      req.Age,
		Password: password,
	}
	s.db[newUser.Email] = newUser

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"ID":    newUser.ID,
		"name":  newUser.Name,
		"email": newUser.Email,
	})
	tokenString, _ := token.SignedString(secret)

	return &pb.RegisterResponse{
		Ok:  true,
		Jwt: tokenString,
	}, nil
}

func (s *AuthService) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	u, ok := s.db[req.GetEmail()]
	if !ok {
		return nil, nil
	}

	mac := hmac.New(sha256.New, secret)
	mac.Write([]byte(req.Password))
	inputPassword := hex.EncodeToString(mac.Sum(nil))

	if inputPassword != u.Password {
		log.Println("Wrong Password")
		return nil, ErrUnauthorized
	}
	log.Println("Password OK")

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"ID":    u.ID,
		"name":  u.Name,
		"email": u.Email,
	})
	tokenString, _ := token.SignedString(secret)

	return &pb.LoginResponse{
		Ok:  true,
		Jwt: tokenString,
	}, nil
}
