FROM golang:alpine as modules

ADD go.mod go.sum /m/
RUN cd /m && go mod download

FROM golang:alpine as builder

COPY --from=modules /go/pkg /go/pkg
RUN adduser -D -u 10001 myapp
RUN mkdir -p /app
ADD . /app
WORKDIR /app
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -o ./bin/main .

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
USER myapp

COPY --from=builder /app/bin/main /app/main

WORKDIR /app
ENV AUTH_SERVICE_GRPC_ADDR=9091
CMD ["/app/main"]
EXPOSE 9091