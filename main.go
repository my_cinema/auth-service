package main

import (
	pb "authmod/api"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	vault "github.com/hashicorp/vault/api"
)

var secret = []byte("")

func main() {

	vaultAddr := os.Getenv("VAULT_ADDR")
	vaultToken := os.Getenv("VAULT_TOKEN")
	authSecretPath := os.Getenv("AUTH_SECRET_PATH")

	fmt.Printf("addr: %s, token: %s, path: %s\n", vaultAddr, vaultToken, authSecretPath)

	config := vault.Config{Timeout: 5 * time.Second}

	err := config.ReadEnvironment()
	if err != nil {
		fmt.Printf("can't read environments: %v\n", err)
		return
	}
	vaultClient, err := vault.NewClient(&config)
	if err != nil {
		fmt.Printf("Can't create vault client instance: %v\n", err)
		return
	}

	authSecret := make(map[string]interface{})
	var isExist bool
	for attempt := 1; attempt < 6; attempt++ {
		fmt.Printf("attemp of connecting to vault ... #: %v\n", attempt)
		authSecretFromVault, err := vaultClient.Logical().Read(authSecretPath)
		if err != nil {
			fmt.Printf("Can't read secret from Vault: %v\n", err)
		}
		if authSecretFromVault != nil {
		authSecret, isExist = authSecretFromVault.Data["data"].(map[string]interface{})
		if isExist {
			break
		} else {
			fmt.Printf("Can't find secret in Vault data\n")
		}}
		time.Sleep(time.Duration(attempt) * time.Second)
	}

	if !isExist {
		fmt.Printf("Can't find auth secret in Vault data\n")
	} else {
		fmt.Printf("auth secret from vault is: %v\n", authSecret["secret"])
	}

	marshalledSecret, err := json.Marshal(authSecret["secret"])
	if err != nil {
		fmt.Printf("Can't parse secret: %v\n", err)
	}

	unqouteSecret, err :=  strconv.Unquote(string(marshalledSecret))

	secret = []byte(unqouteSecret)

	addr := os.Getenv("AUTH_SERVICE_GRPC_ADDR")
	if addr == "" {
		log.Fatal("Unable to retrieve env AUTH_SERVICE_GRPC_ADDR")
	}
	lis, err := net.Listen("tcp", fmt.Sprintf("%s", "auth-service:"+addr))
	//lis, err := net.Listen("tcp", fmt.Sprintf("%s", "0.0.0.0:"+addr))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe("0.0.0.0:8091", nil)
	}()

	s := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionIdle: 5 * time.Minute,
		}),
	)
	pb.RegisterAuthServiceServer(s, NewAuthService())
	log.Println("Serving GRPC on " + addr)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
